import React from "react";
import { Container } from "../../../common/Container";
import "./Header.scss";

const Header = () => {
  return (
    <header>
      <Container>
        <div className="header__block">
          <div className="logo">Logo</div>
          <nav className="nav__menu">
            <ul className="nav__list">
              <li className="nav__list-item">
                <a href="/" className="nav__link">
                  About us
                </a>
              </li>
              <li className="nav__list-item">
                <a href="/" className="nav__link">
                  Why?
                </a>
              </li>
              <li className="nav__list-item">
                <a href="/" className="nav__link">
                  About us
                </a>
              </li>
              <li className="nav__list-item">
                <a href="/" className="nav__link">
                  Stories
                </a>
              </li>
              <li className="nav__list-item">
                <a href="/" className="nav__link">
                  Programs
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </Container>
    </header>
  );
}

export default Header