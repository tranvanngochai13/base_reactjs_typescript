import axiosClient from "./axiosClient";

export const userApi = {
  getUsers() {
    return axiosClient.get("/user");
  },
  getUser(id) {
    return axiosClient.get(`/user/${id}`);
  },
  updateUser(data) {
    return axiosClient.post("/user", data);
  },
};
