import "./App.scss";
import { ScrollToTop } from "./components/ScrollToTop/ScrollToTop";
import { Router } from "./routes/router";

const App:React.FC = () => {
  return (
    <div className="App">
      <ScrollToTop />
      <Router />
    </div>
  );
}

export default App
