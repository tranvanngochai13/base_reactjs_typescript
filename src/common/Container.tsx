import * as React from 'react';

interface IProps   {
  children?: React.ReactNode
}

export const Container:React.FC<IProps > = (props) => {
  return <div className="container">{props.children}</div>;
};


