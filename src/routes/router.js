import React from "react";
import { useRoutes } from "react-router-dom";
import { PATH_NAME } from "../constants/routes";
import AuthLayout from "../layout/AuthLayout";
import MainLayout from "../layout/MainLayout";
import Login from "../pages/Auth/Login";
import SignUp from "../pages/Auth/SignUp";
import Home from "../pages/Home/Home";
import NotFound from "../pages/NotFound/NotFound";

export const Router = () => {
  let element = useRoutes([
    {
      element: <AuthLayout />,
      children: [
        { path: PATH_NAME.LOGIN, element: <Login /> },
        { path: PATH_NAME.SIGNUP, element: <SignUp /> },
      ],
    },
    {
      element: <MainLayout />,
      children: [{ path: PATH_NAME.HOME, element: <Home /> }],
    },
    {
      element: <NotFound />,
      path: "*",
    },
  ]);
  return element;
};
